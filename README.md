# USB Password
- Usb Password es un programa que te permite proteger con contraseña, los datos que tengas almacenados en tu USB.
- Este programa es __Open Source__, por lo tanto si lo deseas, puedes contribuir con el proyecto.
- Para mas información, enviar mensaje al correo: _jusefer2@hotmail.es_
- Desarrollado por Juan Miguel Segura Fernandez

# Instalación
- Para empezar con la instalación, clona o descarga este mismo repositorio.
- Despúes, abra el programa Instrucciones.exe y siga los pasos.
- Finalmente, tendrá en su USB el programa Acceder.exe instalado que será el que le permita acceder a sus archivos del USB con la contraseña que anteriormtente debe haber indicado.

## Capturas

![Imagen no disponible.](img/captura1.PNG)

![Imagen no disponible.](img/captura2.PNG)
