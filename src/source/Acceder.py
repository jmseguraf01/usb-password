#!/usr/bin/env python
# -*- coding: utf-8 -*-

#------------------------------------------------------------------------------------------
# Programa creado por Juan Miguel Segura Fernandez
# Para contactar : jusefer2@hotmail.es
# LICENCIA GPL / GNU
#------------------------------------------------------------------------------------------

from Tkinter import *
from Tkinter import Tk
import ttk
from PIL import ImageTk # pip install Image / # Para importar el modulo de imagenes en WINDOWS
import tkMessageBox, os, time, sqlite3, hashlib, urllib

# Si es la primera vez que se inicia, abro las instrucciones
if os.path.isfile("primeravez.txt"):
	os.system("instrucciones.exe")
	os.system("instrucciones.py")
	sys.exit()

# Creo la interfaz
interfaz=Tk()
interfaz.title("USB Password")
interfaz.geometry("700x320+500+400")
bit = interfaz.iconbitmap("img/candado.ico") # Icono de la interfaz
interfaz.resizable(0,0) # Para que no salga el boton de hacerse grande

# Label del titulo
label_title=Label(interfaz, text="Sistema de Seguridad USB.", font="Helveltica 18 italic", fg="blue", height=2)
label_title.pack()

# Imagen y label del titulo (Security)
imagen_title=ImageTk.PhotoImage(file="img/security.png")
label_imagen_title=Label(interfaz, image=imagen_title)
label_imagen_title.pack()
label_imagen_title.place(x=512, y=0)


# Label de instruccion
label1=Label(interfaz, text="Intruduzca la contraseña para desbloquear el Usb:", font="Helveltica 12")
label1.pack()

# Caja de la contraseña
caja_password=Entry(interfaz, show="*", bd=5)
caja_password.pack()
caja_password.place(x=270 ,y=93)

# Funcion para validarse y abrir el menu del usb
def desencrypt():
	# Obtengo la contraseña que ha introducido el usuario
	contrasena=(caja_password.get())
	# Leo el hash de la contraseña del fichero
	f=open("img/pass", "r")
	hash=f.read()
	f.close()
	# Convierto la contraseña puesta por el usuario a hash y comparo si es igual que el anterior
	password=hashlib.md5(contrasena).hexdigest()

	# Si la contraseña es correcta abro el menu
	if password==hash:
		interfaz.destroy()
		# Defino la interfaz del menu
		interfaz_menu=Tk()
		interfaz_menu.title("Menú")
		interfaz_menu.geometry("400x150+500+400")
		bit = interfaz_menu.iconbitmap("img/accept.ico") # Icono de la interfaz
		interfaz_menu.resizable(0,0) # Para que no salga el boton de hacerse grande

		# Label del titlo de la interfaz menu
		label_title_menu=Label(interfaz_menu, text="Que desea hacer?", height=2, font="Helveltica 12 italic", fg="blue")
		label_title_menu.pack()

		# Funcion que obtiene el resultado de radiobutton y ejecuta la opcion correcta
		def opcion(self):
			# Obtengo la seleccion del combobox
			seleccion=combo.get()
			# Si se selecciona abrir carpeta
			if seleccion=="Abrir carpeta":
				os.system("start Todo") # Abro la carpeta todo, carpeta que contiene todo lo del USB
				sys.exit()

			# Si se selecciona el radiobutton de mostrar carpeta
			elif seleccion=="Mostrar carpeta":
				os.system("attrib -s -h Todo")

			# Si se selecciona el radiobuton de ocultar la carpeta
			elif seleccion=="Ocultar carpeta":
				os.system("attrib +s +h Todo")
				sys.exit()

		# Defino un convo que es un menu desplegable
		combo = ttk.Combobox(interfaz_menu, state="readonly")
		combo.place(x=130, y=50)
		# Le añado los valores que tiene este menu
		combo["values"] = ["Abrir carpeta", "Mostrar carpeta", "Ocultar carpeta"]
		combo.bind("<<ComboboxSelected>>", opcion)

		# Imagen y label de la pregunta
		imagen_pregunta=ImageTk.PhotoImage(file="img/pregunta.png")
		label_pregunta=Label(interfaz_menu, image=imagen_pregunta)
		label_pregunta.pack()
		label_pregunta.place(x=310, y=50)

		# Imagen y label de la pregunta 2
		imagen_pregunta2=ImageTk.PhotoImage(file="img/pregunta2.png")
		label_pregunta2=Label(interfaz_menu, image=imagen_pregunta2)
		label_pregunta2.pack()
		label_pregunta2.place(x=30, y=50)

		interfaz_menu.mainloop()

	# Si la contraseña NO es correcta
	else:
		# Adivino la IP del ordenador donde se ha ejecutado el programa
		ip=urllib.urlopen("http://checkip.dyndns.org").read()
		# Remplazo todo el texto excepto la IP
		ip=ip.replace("<html><head><title>Current IP Check</title></head><body>Current IP Address:","")
		ip=ip.replace("</body></html>","")
		# print "Esta es la ip: %s el dia %s a la hora %s" %(ip, time.strftime("%x"), time.strftime("%X"))
		tkMessageBox.showerror("ERROR","Contraseña incorrecta.")




# Imagen y boton del candado abierto, PARA VALIDAR
boton_check=ttk.Button(interfaz, text="Validar", cursor="hand2", command=desencrypt, width=15)
boton_check.pack()
boton_check.place(x=285, y=130)

# Imagen y label del boton_check
imagen_boton_check=ImageTk.PhotoImage(file="img/check.png")
label_imagen_boton_check=Label(interfaz, image=imagen_boton_check)
label_imagen_boton_check.pack()
label_imagen_boton_check.place(x=260, y=134)

# Boton para salir
boton_exit=ttk.Button(interfaz, style="boton_exit.TButton", text="Salir", cursor="hand2", command=sys.exit, width=15)
boton_exit.pack()
boton_exit.place(x=285, y=160)
# Estilo de boton_exit
ttk.Style().configure("boton_exit.TButton", background="red", foreground="red")
# Imagen y label del boton salir
imagen_x=ImageTk.PhotoImage(file="img/x.png")
label_imagen_x=Label(interfaz, image=imagen_x)
label_imagen_x.pack()
label_imagen_x.place(x=260, y=162)

# Imagen y label del candado( decoracion )
imagen_candado=ImageTk.PhotoImage(file="img/candado.png")
label_candado=Label(interfaz, image=imagen_candado)
label_candado.pack()
label_candado.place(x=10, y=140)

# Imagen y label del candado( decoracion )
imagen_candado2=ImageTk.PhotoImage(file="img/candado.png")
label_candado2=Label(interfaz, image=imagen_candado2)
label_candado2.pack()
label_candado2.place(x=520, y=140)


# ------------------------------------------------------------------
# 								Menu
# ------------------------------------------------------------------
" Funciones a las que se llama desde el menu opciones arriba a la izquierda"

def cambiar_contrasena():
	# Cierro la interfaz principal1
	interfaz.withdraw()
	# Creo una nueva interfaz
	interfaz_cambiar_contrasena=Toplevel()
	interfaz_cambiar_contrasena.title("Cambiar contraseña")
	bit = interfaz_cambiar_contrasena.iconbitmap("img/key.ico")
	interfaz_cambiar_contrasena.geometry("700x320+500+400")
	interfaz_cambiar_contrasena.resizable(0,0)
	# Cuando cierro esta ventana, la ventana principal se muestra
	def exit():
		interfaz_cambiar_contrasena.destroy()
		interfaz.deiconify()
	interfaz_cambiar_contrasena.protocol("WM_DELETE_WINDOW", exit)

	# Funcion para comprobar la contraseña actual si es correca (se inicia cuando se le da click a la caja_nueva_contrasena)
	def comprobar_contrasena_actual(event):
		contrasena_actual=caja_contrasena_antigua.get()
		# Leo el hash de la contraseña del fichero
		f=open("img/pass", "r")
		hash=f.read()
		f.close()

		# Convierto la contraseña puesta por el usuario a hash y comparo si es igual que el anterior
		password=hashlib.md5(contrasena_actual).hexdigest()

		# Si la contraseña actual es correcta oculto la label label_contrasena_incorrecta y muestro la label label_contrasena_correcta
		if password==hash:
			label_contrasena_incorrecta.place_forget()
			label_contrasena_correcta.place(x=10, y=125)

		# Si la contraseña actual es incorrecta muestro la label label_contrasena_incorrecta y oculto label_contrasena_correcta
		else:
			label_contrasena_correcta.place_forget()
			label_contrasena_incorrecta.place(x=10, y=125)
	# ------------------------------------------
	# FINAL FUNCION comprobar_contrasena_actual


	# Funcion para actualizar la contraseña
	def actualizar_contrasena():
		# Compruebo si la contraseña actual es correcta o no
		contrasena_actual=caja_contrasena_antigua.get()
 		# Leo el hash de la contraseña del fichero
 		f=open("img/pass", "r")
 		hash=f.read()
 		f.close()
 		# Convierto la contraseña puesta por el usuario a hash y comparo si es igual que el anterior
 		password=hashlib.md5(contrasena_actual).hexdigest()

		# Si la contraseña es correcta
 		if password==hash:
			nueva_contrasena=caja_nueva_contrasena.get()
			repetir_nueva_contrasena=caja_nueva_contrasena2.get()
 			# Compruebo si la nueva contraseña y repetir la nueva contraseña coincide
			if nueva_contrasena == repetir_nueva_contrasena and nueva_contrasena != "" and repetir_nueva_contrasena != "":
				# Convierto la nueva contraseña en hash
				password=hashlib.md5(nueva_contrasena).hexdigest()
				# Abro el txt en modo lectura y sustituyo el texto anterior por hash de la nueva_contrasena
				f=open("img/pass", "w")
				f.write(password)
				f.close()
				tkMessageBox.showinfo("EXITO", "La contraseña ha sido actualizada con exito!")
				exit()

			# Si la nueva contraseña esta en blanco
			elif nueva_contrasena == "":
				tkMessageBox.showerror("ERROR","La nueva contraseña no puede estar en blanco.")

			# Si repetir la nueva contraseña esta en blanco
			elif repetir_nueva_contrasena == "":
				tkMessageBox.showerror("ERROR","Debe repetir la nueva contraseña.")

			# Si la nueva contraseña y repetir nueva contraseña no coinciden
			else:
				tkMessageBox.showerror("ERROR","La nueva contraseña no coinciden.")

 		# Si la contraseña actual no es correcta
 		else:
 			tkMessageBox.showerror("ERROR","La contraseña actual no es correcta.")



	# Label del titulo
	label_title=Label(interfaz_cambiar_contrasena, text="Cambiar contraseña.", font="Helveltica 18 italic", fg="blue", height=2)
	label_title.pack()

	# Label y caja de la contraseña antigua
	label_contrasena_antigua=Label(interfaz_cambiar_contrasena, text="Escriba la contraseña actual ", font="Helveltica 12 italic")
	label_contrasena_antigua.pack()
	label_contrasena_antigua.place(x=10, y=70)
	caja_contrasena_antigua=ttk.Entry(interfaz_cambiar_contrasena, width=22, font="Helvetica 12", show="*")
	caja_contrasena_antigua.pack()
	caja_contrasena_antigua.place(x=13, y=100)

	# Defino la label de si la contraseña actual esta mal y la oculto
	label_contrasena_incorrecta=Label(interfaz_cambiar_contrasena, text="La contraseña actual no es correcta.", fg="red")
	label_contrasena_incorrecta.pack()
	label_contrasena_incorrecta.forget()

	# Label y caja de la nuvea contrasena
	label_nueva_contrasena=Label(interfaz_cambiar_contrasena, text="Escriba la nueva contrasena", font="Helveltica 12 italic", fg="black")
	label_nueva_contrasena.pack()
	label_nueva_contrasena.place(x=420, y=70)
	caja_nueva_contrasena=ttk.Entry(interfaz_cambiar_contrasena, width=22, font="Helvetica 12", show="*")
	caja_nueva_contrasena.pack()
	caja_nueva_contrasena.place(x=420, y=100)
	# Cuando se le da click a la caja de la nueva contraseña, se llama a la funcion de comprobar si la contraseña actual es correcta
	caja_nueva_contrasena.bind("<1>", comprobar_contrasena_actual)

	# Defino la label de si la contraseña actual esta bien y la oculto
	label_contrasena_correcta=Label(interfaz_cambiar_contrasena, text="La contraseña actual es correcta.", fg="#018523")
	label_contrasena_correcta.pack()
	label_contrasena_correcta.forget()

	# Segunda label y caja de la nuvea contrasena
	label_nueva_contrasena2=Label(interfaz_cambiar_contrasena, text="Repita la nueva contrasena", font="Helveltica 12 italic", fg="black")
	label_nueva_contrasena2.pack()
	label_nueva_contrasena2.place(x=420, y=150)
	caja_nueva_contrasena2=ttk.Entry(interfaz_cambiar_contrasena, width=22, font="Helvetica 12", show="*")
	caja_nueva_contrasena2.pack()
	caja_nueva_contrasena2.place(x=420, y=180)
	caja_nueva_contrasena2.bind("<1>", comprobar_contrasena_actual)

	# Boton para actualizar contraseña
	boton_actualizar_contrasena=ttk.Button(interfaz_cambiar_contrasena, text="Actualizar contraseña", width=20, cursor="hand2", command=actualizar_contrasena)
	boton_actualizar_contrasena.pack()
	boton_actualizar_contrasena.place(x=460, y=220)

	# Imagen y boton volver
	imagen_boton_volver=ImageTk.PhotoImage(file="img/volver.png")
	boton_volver=Button(interfaz_cambiar_contrasena, image=imagen_boton_volver, cursor="hand2", borderwidth=0, command=exit)
	boton_volver.pack()
	boton_volver.place(x=20, y=240)

	# Imagen y label de la imagen de las llaves
	imagen_cambiar_contrasena=ImageTk.PhotoImage(file="img/cambiar_contrasena.png")
	label_imagen_cambiar_contrasena=Label(interfaz_cambiar_contrasena, image=imagen_cambiar_contrasena)
	label_imagen_cambiar_contrasena.pack()
	label_imagen_cambiar_contrasena.place(x=250, y=70)

	interfaz_cambiar_contrasena.mainloop()


# Actualizar el programa ( aun no funciona )
def update():
	import git
	try:
		repo=git.Repo(".")
		pull=repo.git.pull()
		# Si no hay otra versión disponible
		if pull=="Already up to date.":
			tkMessageBox.showerror("Error","No hay actualizaciones disponibles.")
		else:
			tkMessageBox.showinfo("Update", pull)

	# Si no tiene el repositorio .git y no se puede actualizar
	except:
		tkMessageBox.showerror("Error","Esta versión no se puede actualizar. Para más información consulte con el administrador.")


# Funcion que lee el txt y lo muestra en un canvas
def see_license():
	# Importo esta librearia para leer la pagina web donde esta la licencia
	import urllib2
	# Abro la conexion con la pagina, leo el contenido y cierro la conexion
	f = urllib2.urlopen("https://www.gnu.org/licenses/gpl.txt")
	leer_licencia=f.read()
	f.close()

	# Creo una interfaz
	interfaz_license=Tk()
	interfaz_license.geometry("580x770")
	interfaz_license.title("License")
	interfaz_license.resizable(0,0)
	interfaz_license.config(bg="white")
	# Creo el canvas
	frame=Frame(interfaz_license,width=550,height=750)
	frame.grid(row=0,column=0)
	canvas=Canvas(frame,bg='#FFFFFF',width=900,height=900,scrollregion=(0,0,0,10280))
	# Scrollbar de Y para subir y bajar
	vbar=Scrollbar(frame,orient=VERTICAL)
	vbar.pack(side=RIGHT,fill=Y)
	vbar.config(command=canvas.yview)
	# Configuracion del canvas
	canvas.config(width=550,height=750)
	canvas.config( yscrollcommand=vbar.set)
	canvas.pack(side=LEFT,expand=True,fill=BOTH)
	# Label de la licencia
	canvas.create_text(20, 20, anchor='nw', text=leer_licencia)

	interfaz_license.mainloop()



# Funcion que te manda a la pagina web para ver el código fuente
def see_code_font():
	# Modulo para abrir una web en el navegador
	import webbrowser
	tkMessageBox.showinfo("Atención","Se abrirá la pagina web del proyecto en tu navegador.")
	webbrowser.open("http://www.gitlab.com/jmseguraf01/usb-password", new=2, autoraise=True)


# Funcion que muestra informacion sobre el programa y sobre mi
def acerca_de():
	interfaz.withdraw()
	# Creo la interfaz acerca_de
	interfaz_acerca_de=Toplevel()
	interfaz_acerca_de.title("Acerca de")
	interfaz_acerca_de.resizable(0,0)
	interfaz_acerca_de.geometry("700x320+500+400")
	bit = interfaz_acerca_de.iconbitmap("img/info.ico") # Icono de la interfaz
	# Cuando cierro esta ventana, la ventana principal se muestra
	def exit():
		interfaz_acerca_de.destroy()
		interfaz.deiconify()
	interfaz_acerca_de.protocol("WM_DELETE_WINDOW", exit)

	# Label del titulo de acerca de
	title_acercade=Label(interfaz_acerca_de, text="USB Pasword", fg="#3293A4", font=("Helveltica 20 italic"), height=2)
	title_acercade.pack()

	# Creo un canvas
	canvas = Canvas(interfaz_acerca_de, height=50)
   	canvas.pack(fill='both', expand=True)

	# Canvas de la informacion de acerca de
	canvas.create_text(380, 50, width=600, font=("Helvetica 12"), text="USB Password es un programa Open Source, que permite almacenar todos los datos que contiene en el USB, protegidos con una contraseña. Usted puede contribuir con el proyecto en gitlab. \nSi desea contactar con el administrador puede mande un correo a la dirección: jusefer2@hotmail.es.")
	# Canvas autor
	canvas.create_text(175, 110, text="Proyecto desarrollado por:", font=("Helvetica 12"))
	canvas.create_text(320, 130, text="Juan Miguel Segura Fernandez ( https://gitlab.com/jmseguraf01 )", fill="blue", font=("Helvetica 11 italic"))
	# Canvas version
	canvas.create_text(80, 220, text="Versión 1.2", font=("Helvetica 15 italic"), fill="red")

	# Imagen y label de opensource
	imagen_opensource=ImageTk.PhotoImage(file="img/opensource.png")
	canvas.create_image(620, 180, image=imagen_opensource)

	# Imagen y label de gitlab
	imagen_gitlab=ImageTk.PhotoImage(file="img/gitlab.png")
	canvas.create_image(350, 200, image=imagen_gitlab)

	interfaz_acerca_de.mainloop()



# Defino el menu y se lo añado a la interfaz
menu=Menu(interfaz)
interfaz.config(menu=menu)
# Creo el filemenu
filemenu = Menu(menu, tearoff=0, bg="#e1e1e1")
# Añado el titulo  al menu
menu.add_cascade(label="Opciones", menu=filemenu)
filemenu.add_command(label="Cambiar contraseña", command=cambiar_contrasena)
filemenu.add_command(label="Actualizar", command=update)
filemenu.add_separator() # Separador
# Ver licencia
filemenu.add_command(label="Ver licencia", command=see_license)
# Ver codigo fuente
filemenu.add_command(label="Ver código fuente", command=see_code_font)
filemenu.add_separator() # Separador
# Acerca del programa
filemenu.add_command(label="Acerca de USB Password", command=acerca_de)



interfaz.mainloop()
