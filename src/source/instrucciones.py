#!/usr/bin/env python
# -*- coding: utf-8 -*-

#------------------------------------------------------------------------------------------
# Programa creado por Juan Miguel Segura Fernandez
# Para contactar : jusefer2@hotmail.es
# LICENCIA GPL / GNU
#--------------------------

"""
Instrucciones del programa Acceder.py
"""
from Tkinter import *
from Tkinter import Tk
import ttk
from PIL import ImageTk
import tkMessageBox, os, time, tkFileDialog, commands, hashlib
from cryptography.fernet import Fernet

# Interfaz de instrucciones
interfaz=Tk()
interfaz.title("Instrucciones")
interfaz.geometry("700x350+500+400")
bit = interfaz.iconbitmap("img/instrucciones.ico") # Icono de la interfaz
interfaz.config(bg="white")
interfaz.resizable(0,0)

# Label del titulo
instrucciones_title=Label(interfaz, text="Instrucciones", font=("Helveltica 20 italic"), fg="blue", height=2, bg="white")
instrucciones_title.pack()

# Creo un canvas
canvas=Canvas(bg="white")
# Configuracion del canvas
canvas.config()
canvas.pack(side=LEFT,expand=True,fill=BOTH)
# Canvas linea debajo titulo
canvas.create_line(0, 2, 700, 2, fill="blue", width=2.5)
# Label de bienvenido
canvas.create_text(20, 20, anchor='nw', width=650, text="Bienvenido!", fill="blue", font=("Helvetica 12 underline italic"))
canvas.create_text(20, 60, anchor='nw', width=650, font=("Helvetica 12"), text="Este programa permite proteger con una contraseña los datos que hay en un USB. Para empezar, escriba la contraseña con la que desea acceder a sus datos, después, conecte el USB a tu ordenador y pulse el botón de 'Seleccionar USB'.")
canvas.create_text(20, 250, anchor='nw', width=650, text="Este programa esta bajo la licencia de software libre.", fill="#002558", font=("Helvetica 11 italic"))

# Funcion para seleccionar el USB
def seleccionar_usb():
    # File Dialog para seleccionar el usb
    usb=tkFileDialog.askdirectory(title="Seleccione el USB donde desea instalar el programa. ", initialdir="/")
    # Escribo los datos del directorio en un txt
    f=open("directory.txt","w")
    f.write(usb)
    f.close()
    # Remplazo / por \\ para que no de errores en la ruta
    a=open("directory.txt","r")
    usb=a.read()
    usb=usb.replace("/","\\")
    a.close()
    # Elimino el fichero
    os.system("del directory.txt")

    # Creo la carpeta Todo
    try:
        os.mkdir(""+usb+"/Todo")
        noexiste=False

    # Si ya existe la carpeta
    except WindowsError:
        tkMessageBox.showerror("Error.","Ya existe una carpeta con el nombre 'Todo'. Elimina la carpeta o cambiale el nombre.")
        noexiste=True

    # Si no existe la carpeta Todo
    if noexiste==False:
        # Miro los archivos que hay dentro del usb
        listdir=os.listdir(usb)
        # Añado cada archivo a la carpeta "Todo"
        for file in listdir:
            os.system("move "+usb+"\\"+file+" "+usb+"\\Todo")

        # Copio el programa Acceder y la carpeta de img al usb y oculto la carpeta Todo y la carpeta img
        os.system("copy Acceder.py "+usb+"")
        os.system("copy Acceder.exe "+usb+"")
        os.system("mkdir "+usb+"\\img")
        os.system("copy img "+usb+"\\img")
        os.system("attrib +s +h "+usb+"\\Todo")
        os.system("attrib +s +h "+usb+"\\img")

        # Si se ha creado bien la carpeta y contiene ficheros dentro
        if os.path.isdir(""+usb+"\\Todo"):
            tkMessageBox.showinfo("Instalado.", "El programa se ha instalado correctamente. Vaya a su USB y inicie el programa 'Acceder.'")
            sys.exit()
        # Si no se ha creado la carpeta, o ha habido un error
        else:
            tkMessageBox.showerror("Error.","Ha habido un error interno y no se ha configurado correctamente. Intentalo de nuevo o contacte con el administrador.")

# Funcion para guardar la contrasena
def guardar_contrasena():
    try:
        # Contraseña
        contrasena=caja_contrasena.get()
        # Si se introduce una contraseña
        if contrasena != "":
            save_password=tkMessageBox.askyesno("Confirmar.","Desea establecer como contraseña: "+contrasena+"?")
            # Si se guarda la contraseña
            if save_password==True:
                # Creo un hash de la contraseña introducida
                password=hashlib.md5(contrasena).hexdigest()
                print password
                # Escribo el hash en el txt
                f=open("img/pass", "w")
                f.write(password)
                f.close()
                a=open("img/pass", "r")
                read=a.read()
                a.close()
                # Una vez guardado el hash en el archivo, llamo a la funcion para seleccionar el usb
                seleccionar_usb()

            # Si se da a no guardar la contraseña
            elif save_password==False:
                pass

        # Si no se ha introducido nada como contraseña
        else:
            tkMessageBox.showerror("Error.","No se ha introducido ninguna contraseña.")

    # Si hay un caracter raro en la contraseña
    except UnicodeDecodeError:
        tkMessageBox.showerror("Error.","La contraseña tiene un caracter que no se es válido.")

# Label contraseña.
label_contrasena=Label(interfaz, text="Introduzca una contraseña:", bg="white", font="Helvetica 12 italic", fg="#0b18bb")
label_contrasena.pack()
label_contrasena.place(x=20, y=200)

# Caja para guardar contraseña
caja_contrasena=Entry(interfaz, bd=1, bg="#ccf9ff", font="Helveltica 10", width=25,  highlightbackground="blue", highlightcolor="blue", highlightthickness=1)
caja_contrasena.place(x=22, y=230)
# Boton guardar contraseña
boton_guardar_contrasena=ttk.Button(interfaz, text="Guardar contraseña", cursor="hand2", width=21, style="a.TButton", command=guardar_contrasena)
boton_guardar_contrasena.pack()
boton_guardar_contrasena_style=ttk.Style()
boton_guardar_contrasena_style.configure("a.TButton", font="Helveltica 10")
boton_guardar_contrasena.place(x=27, y=260)

# Imagen y label de instrucciones
imagen_instrucciones=ImageTk.PhotoImage(file="img/instrucciones.png")
label_img_instrucciones=Label(interfaz, image=imagen_instrucciones, bg="white")
label_img_instrucciones.pack()
label_img_instrucciones.place(x=500, y=195)

os.system("del primeravez.txt")

interfaz.mainloop()
